const state = {
    baseURL:'https://localhost:5001',
    // baseURL:'https://api.yell.run',
	uploadURL: '/api/file/upload/',
}
const mutations = {
}
const actions = {
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}