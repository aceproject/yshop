import request from '@/utils/request'
import store from '@/store';


export function upload(directory){
    return request({
        url: store.state.base.uploadURL + directory,
        method: 'post'
    })
}