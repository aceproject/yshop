import request from '@/utils/request'
import qs from 'querystring'

export function login(data){
    return request({
        url: '/connect/token',
        method: 'post',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        data: qs.stringify(data)
    })
}