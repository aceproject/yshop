import request from '@/utils/request'

/**
 * 商品分类
 */


/**
 * 获取商品分类
 * @param {any}} search 
 * @returns 
 */
 export function getList(search){
    return request({
        url: '/api/product-categroy/list',
        method: 'get',
        params:search
    })
}

/**
 * 根据Id获取商品分类
 * @param {string} id 
 * @returns 
 */
export function get(id){
    return request({
        url: '/api/product-categroy/' + id,
        method: 'get'
    })
}
/**
 * 更新商品分类
 * @param {object} update 
 * @returns 
 */
export function update(update){
    return request({
        url: '/api/product-categroy',
        method: 'put',
        data:update
    })
}
/**
 * 插入商品分类
 * @param {object} insert 
 * @returns 
 */
export function insert(insert){
    return request({
        url: '/api/product-categroy',
        method: 'post',
        data:insert
    })
}

export function remove(id){
    return request({
        url: '/api/product-categroy/' + id,
        method: 'delete',
    })
}