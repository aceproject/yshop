import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    component: () => import('@/views/system/login')
  },
  // {
  //   path: '/regist',
  //   component: () => import('@/views/system/regist')
  // },
  // {
  //   path: '/404',
  //   component: () => import('@/views/system/404')
  // },
  // {
  //   path: '/401',
  //   component: () => import('@/views/system/401')
  // },
  // {
  //   path: '/',
  //   name: 'Home',
  //   redirect: 'desktop',
  //   component: home,
  //   children: [
  //     {
  //       path: 'desktop',
  //       component: () => import('@/views/system/desktop'),
  //     },
  //     {
  //       path: 'sample',
  //       component: () => import('@/views/sample/index')
  //     },
  //     {
  //       path: 'sample/add',
  //       component: () => import('@/views/sample/add')
  //     },
  //     {
  //       path: 'sample/edit',
  //       component: () => import('@/views/sample/edit')
  //     },
  //     {
  //       path: 'menu',
  //       component: () => import('@/views/system/menu/index')
  //     },
  //     {
  //       path: 'menu/form',
  //       component: () => import('@/views/system/menu/form')
  //     },
  //   ]
  // },
]

const router = new VueRouter({
  routes
})

export default router
