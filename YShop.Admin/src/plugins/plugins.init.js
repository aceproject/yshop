import Vue from 'vue'
import './element/element.js'
import './vxe-table/vxe-table' 
// import './vuescroll/vuescroll'

import lscache from 'lscache'
var lodash = require('lodash')

$y['_'] = lodash
$y['cache'] = lscache
$y['toQueryParams'] = function(urlParam){
    if(!urlParam)
        return ''
    return JSON.stringify(urlParam)
                .replace(/"|\{|\}/g,'')
                .replace(/,/g,'&')
                .replace(/:/g,'=')
}


import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style

$y['progress'] =  NProgress