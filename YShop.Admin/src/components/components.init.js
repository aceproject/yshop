import Vue from 'vue'

const layoutComponents = require.context('./common', true, /\.vue$/)
const modules = layoutComponents.keys().reduce((conponents, componentPath) => {
  // set './app.js' => 'app'
  // const componentName = componentPath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const component = layoutComponents(componentPath)
  if(!component.default.name){
    console.error(`路径:${componentPath} 组件名称为空！`)
    return ;
  }
  Vue.component(component.default.name,component.default)
  return conponents
}, {})