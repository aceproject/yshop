﻿using Furion.FriendlyException;
using System.ComponentModel;

namespace YShop
{
    /// <summary>
    /// 业务错误码
    /// </summary>
    [ErrorCodeType]
    public enum ErrorCodes
    {
        /// <summary>
        /// 用户错误
        /// </summary>
        [Description("用户错误"), ErrorCodeItemMetadata("用户错误")]
        e9000,
        /// <summary>
        /// 密码错误
        /// </summary>
        [Description("密码错误"), ErrorCodeItemMetadata("密码错误")]
        e9001,
        /// <summary>
        /// 用户不存在
        /// </summary>
        [Description("用户不存在"), ErrorCodeItemMetadata("用户不存在")]
        e9002,
        /// <summary>
        /// 主键为空
        /// </summary>
        [Description("主键为空"), ErrorCodeItemMetadata("主键为空")]
        e9003,
        /// <summary>
        /// 数据不存在
        /// </summary>
        [Description("数据不存在"), ErrorCodeItemMetadata("数据不存在")]
        e9004,
        /// <summary>
        /// 删除错误
        /// </summary>
        [Description("删除错误"), ErrorCodeItemMetadata("删除错误")]
        e9005,
        /// <summary>
        /// 缓存键无效
        /// </summary>
        [Description("缓存键无效"), ErrorCodeItemMetadata("缓存键无效")]
        e9006,
        /// <summary>
        /// Id不能为空
        /// </summary>
        [Description("Id不能为空"), ErrorCodeItemMetadata("Id不能为空")]
        e9007,
        /// <summary>
        /// 非法数据
        /// </summary>
        [Description("非法数据"), ErrorCodeItemMetadata("非法数据")]
        e9999,
    }
}
