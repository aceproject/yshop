﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core
{
    /// <summary>
    /// 数据项类型
    /// </summary>
    public enum DataItemType
    {
        /// <summary>
        /// 字典
        /// </summary>
        Dict,
        /// <summary>
        /// 数据源
        /// </summary>
        Source,
        /// <summary>
        /// 系统字典
        /// </summary>
        System
    }
}
