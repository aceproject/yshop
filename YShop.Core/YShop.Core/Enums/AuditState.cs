﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core
{
    /// <summary>
    /// 审核状态
    /// </summary>
    public enum AuditState
    {
        /// <summary>
        /// 待审核
        /// </summary>
        Auditing,
        /// <summary>
        /// 已审核
        /// </summary>
        Audited
    }
}
