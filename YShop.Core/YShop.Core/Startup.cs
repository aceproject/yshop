﻿using Furion;
using Microsoft.Extensions.DependencyInjection;

namespace YShop.Core
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(new DatabaseManager());
            MapsterConfigure(services);
        }

        private void MapsterConfigure(IServiceCollection services)
        {
            //TypeAdapterConfig
            //    .GlobalSettings
            //    .Default
            //    .AddDestinationTransform((string x) => x == "Id" && string.IsNullOrWhiteSpace(x) ? Utils.NewGuid() : x)
            //    .MapToConstructor(true)
            //    .PreserveReference(true);
        }
    }
}
