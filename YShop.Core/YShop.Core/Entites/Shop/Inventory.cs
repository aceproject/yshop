﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core.Entites
{
    /// <summary>
    /// 库存
    /// </summary>
    public class Inventory : BaseEntity
    {
        /// <summary>
        /// SKU 编号
        /// </summary>
        public string ProductDetailCode { get; set; }
        /// <summary>
        /// SKU Id
        /// </summary>
        public string ProductDetailId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }
        /// <summary>
        /// 仓库（用编号）
        /// </summary>
        public string Warehouse { get; set; }
        /// <summary>
        /// 库位（用编号）
        /// </summary>
        public string Location { get; set; }
    }
}
