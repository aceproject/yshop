﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace YShop.Core.Entites.Shop
{
    /// <summary>
    /// 产品选项
    /// </summary>
    public class Attribute : BaseEntity, IEntitySeedData<Attribute>
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选项 
        /// </summary>
        public List<AttributeDetail> Items { get; set; }
        /// <summary>
        /// 选项连接
        /// </summary>
        [Column(TypeName = "text")]
        public string ItemJoin { get; set; }

        public IEnumerable<Attribute> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<Attribute> 
            {
                new Attribute
                { 
                    Code = "A0001",
                    Id = "60435ff9-1293-0b00-0085-31265720eaa2",
                    Name = "颜色",
                }
            };
        }
    }

    /// <summary>
    /// 选项子项
    /// </summary>
    public class AttributeDetail : OnlyPrimaryEntity, IEntitySeedData<AttributeDetail>
    {
        /// <summary>
        /// 属性ID
        /// </summary>
        public string AttributeId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 参数
        /// </summary>
        public string Params { get; set; }

        public IEnumerable<AttributeDetail> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<AttributeDetail> 
            {
                new AttributeDetail { Id = "6043629a-370b-e14c-00ee-551d16c31868", Name = "红", AttributeId = "60435ff9-1293-0b00-0085-31265720eaa2" },
                new AttributeDetail { Id = "604362c9-370b-e14c-00ee-551e5d44baea", Name = "黄", AttributeId = "60435ff9-1293-0b00-0085-31265720eaa2" },
                new AttributeDetail { Id = "604362d3-370b-e14c-00ee-551f21dd3921", Name = "蓝", AttributeId = "60435ff9-1293-0b00-0085-31265720eaa2" },
                new AttributeDetail { Id = "604362dd-370b-e14c-00ee-55202daadad1", Name = "黑", AttributeId = "60435ff9-1293-0b00-0085-31265720eaa2" },
                new AttributeDetail { Id = "604362e6-370b-e14c-00ee-5521547a322c", Name = "灰", AttributeId = "60435ff9-1293-0b00-0085-31265720eaa2" },
            };
        }
    }
}
