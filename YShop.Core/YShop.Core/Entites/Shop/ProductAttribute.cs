﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Furion.DatabaseAccessor;

namespace YShop.Core.Entites.Shop
{
    /// <summary>
    /// 产品属性中间表
    /// </summary>
    public class ProductAttribute : OnlyPrimaryEntity
    {
        public string ProductId { get; set; }
        public string AttributeId { get; set; }
        public string AttributeDetailId { get; set; }
    }
}
