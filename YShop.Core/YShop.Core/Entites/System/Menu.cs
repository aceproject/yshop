﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace YShop.Core.System
{
    [Table("Sys_Menu")]
    public class Menu: BaseEntity, IEntitySeedData<Menu>, IEntityTypeBuilder<Menu>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [MaxLength(32)]
        public string Name { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        [MaxLength(32)]
        public string Icon { get; set; }
        /// <summary>
        /// 菜单类型
        /// Root 根菜单
        /// Group 二级菜单、这里是作为分组
        /// View 视图菜单
        /// </summary>
        public MenuType? Type { get; set; }
        /// <summary>
        /// 父级Id
        /// 最顶级的父Id为 '0'
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// 路由外键
        /// </summary>
        public string RouteId { get; set; }
        /// <summary>
        /// 路由
        /// </summary>
        public Route Route { get; set; }

        public void Configure(EntityTypeBuilder<Menu> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
        }

        public IEnumerable<Menu> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<Menu>
            {
                new Menu{ Name = "系统管理",Type = MenuType.Root ,Id = "5fefa999-e659-9c58-005c-4f70414fd40c",ParentId = "0" },
                new Menu{ Name = "功能",Type = MenuType.Group ,Id = "5fefa9e1-e659-9c58-005c-4f715d544b6f",ParentId = "5fefa999-e659-9c58-005c-4f70414fd40c"},
                new Menu{ Name = "菜单管理",Type = MenuType.View,Id = "5fefa9f5-e659-9c58-005c-4f7275f0545e" ,ParentId = "5fefa9e1-e659-9c58-005c-4f715d544b6f",RouteId = "5ff05630-b33d-a7c8-0014-effb6f4d89ab"},
                new Menu{ Name = "数据管理",Type = MenuType.View,Id = "60044058-c58e-7878-0006-1d8f6919f09a" ,ParentId = "5fefa9e1-e659-9c58-005c-4f715d544b6f",RouteId = "5ff23e0e-8f52-f338-00b5-894b4613aac7"},


                new Menu{ Name = "示例",Type = MenuType.Root,Id = "5ff0d897-b216-a6cc-0000-177979a1dda4",ParentId = "0" ,RouteId = "5ff0c7fd-c953-f494-00b4-329b51a095c2"},
                
                
                new Menu{ Name = "敏捷开发",Type = MenuType.Root,Id = "600460df-cdbb-986c-00dd-53226ab1fdec",ParentId = "0"},
                new Menu{ Name = "功能",Type = MenuType.Root,Id = "6004ae7f-436d-2cac-00f3-016e08619bd1",ParentId = "600460df-cdbb-986c-00dd-53226ab1fdec",},

                new Menu{ Name = "代码生成器",Type = MenuType.Root,Id = "6004af25-436d-2cac-00f3-01706ee73208",ParentId = "6004ae7f-436d-2cac-00f3-016e08619bd1",RouteId = "6004ad61-436d-2cac-00f3-016d2eeb451a"},

                new Menu{ Name = "数据库管理",Type = MenuType.Root,Id = "6004af55-436d-2cac-00f3-01714b1cf0ee",ParentId = "6004ae7f-436d-2cac-00f3-016e08619bd1",RouteId = "6004aefe-436d-2cac-00f3-016f0bae52ef"},
                new Menu{ Name = "其他",Type = MenuType.Root,Id = "6032db19-1281-068c-002a-de3713e238d3",ParentId = "6004ae7f-436d-2cac-00f3-016e08619bd1",RouteId = "6032db4e-1281-068c-002a-de3840bef1b5"},


                new Menu{ Name = "商城管理",Type = MenuType.Root ,Id = "6032d8cd-cda1-81d4-0058-618532ba6418",ParentId = "0" },
                new Menu{ Name = "功能",Type = MenuType.Group ,Id = "6032d8fe-cda1-81d4-0058-61861ff1b22f",ParentId = "6032d8cd-cda1-81d4-0058-618532ba6418"},

                new Menu{ Name = "商品列表",Type = MenuType.View,Id = "6032d910-cda1-81d4-0058-61875ae5e5a6" ,ParentId = "6032d8fe-cda1-81d4-0058-61861ff1b22f",RouteId = "6032da9c-cf82-c8a8-00dd-939527afcbdc"},
                new Menu{ Name = "商品分类",Type = MenuType.View,Id = "60590a4f-5def-4ebf-00c2-fbbb6d4f16e6" ,ParentId = "6032d8fe-cda1-81d4-0058-61861ff1b22f",RouteId = "6059063a-5def-4ebf-00c2-fbb962e274e4"},
                new Menu { Name = "商品属性",Type = MenuType.View,Id = "60435b90-99fc-0d9c-00db-80f141ece2b6",ParentId = "6032d8fe-cda1-81d4-0058-61861ff1b22f",RouteId = "60435bb1-99fc-0d9c-00db-80f25a6c73ad" },
                new Menu{ Name = "订单列表",Type = MenuType.View,Id = "6032d95c-cda1-81d4-0058-6189160a9104" ,ParentId = "6032d8fe-cda1-81d4-0058-61861ff1b22f",RouteId = "6032dab0-cf82-c8a8-00dd-93977c537d9f"},
                new Menu{ Name = "库存列表",Type = MenuType.View,Id = "6032d968-cda1-81d4-0058-618a6bbf36e8" ,ParentId = "6032d8fe-cda1-81d4-0058-61861ff1b22f",RouteId = "6032dac1-cf82-c8a8-00dd-93993e91bdc7"},
                new Menu{ Name = "仓库列表",Type = MenuType.View,Id = "6032d972-cda1-81d4-0058-618b30e08535" ,ParentId = "6032d8fe-cda1-81d4-0058-61861ff1b22f",RouteId = "6032dad2-cf82-c8a8-00dd-939b29863f81"},
                new Menu{ Name = "支付清单",Type = MenuType.View,Id = "6032d97c-cda1-81d4-0058-618c61b30937" ,ParentId = "6032d8fe-cda1-81d4-0058-61861ff1b22f",RouteId = "5ff05630-b33d-a7c8-0014-effb6f4d89ab"},

            };
        }
    }
}
