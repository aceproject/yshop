﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop
{
    public class PageSearchDto
    {
        /// <summary>
        /// 页码
        /// 默认-1
        /// </summary>
        [Range(1, int.MaxValue)]
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// 页容量
        /// 默认-100
        /// </summary>
        [Range(1, int.MaxValue)]
        public int PageSize { get; set; } = 26;
    }
}
