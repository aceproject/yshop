﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YShop.Application.Contracts.System.Routes;
using YShop.Core;

namespace YShop.Application.Contracts.System.Menus
{
    public class MenuDto : BaseEntityDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 菜单类型
        /// Root 根菜单
        /// Group 二级菜单、这里是作为分组
        /// View 视图菜单
        /// </summary>
        public MenuType? Type { get; set; }
        /// <summary>
        /// 父级Id
        /// 最顶级的父Id为 '0'
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// 路由
        /// </summary>
        public RouteDto Route { get; set; }
        /// <summary>
        /// 是否有子项
        /// </summary>
        public bool HasChild { get; set; } = true;
    }
}
