﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YShop.Core.System;

namespace YShop.Application.Contracts.Shop
{
    public class ProductCategroyDto:BaseEntityDto
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 产品分类名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string Picture { get; set; }
    }
}
