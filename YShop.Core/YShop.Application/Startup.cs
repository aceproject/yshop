﻿using Furion;
using Mapster;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using YShop.Application.System;

namespace YShop.Application
{
    public class Startup: AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(new SystemDataItemService());
            TypeAdapterConfig.GlobalSettings.Default.PreserveReference(true);

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime hostApplicationLifetime)
        {
            hostApplicationLifetime.ApplicationStarted.Register(() =>
            {
            });
        }
    }
}
