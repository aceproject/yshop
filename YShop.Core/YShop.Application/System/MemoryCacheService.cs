﻿using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Microsoft.AspNetCore.Mvc;
using YShop.Application.Contracts.Caches;
using YShop.Core.Managers.Caches;

namespace YShop.Application.System
{
    /// <summary>
    /// 内存缓存
    /// </summary>
    [ApiDescriptionSettings("System", Name = "Cache")]
    public class MemoryCacheService : IMemoryCacheService, IDynamicApiController, ISingleton
    {
        /// <summary>
        /// 缓存管理
        /// </summary>
        private readonly CacheManager _cacheManager;

        public MemoryCacheService(CacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        public void Clear()
        {
            _cacheManager.Clear();
        }
    }
}
